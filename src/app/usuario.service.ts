import { Injectable } from '@angular/core';
import { Usuario } from './usuario';
import { UsuarioRoutingModule } from './usuario/usuario.routing.module';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor() { }

  public getUsuario(): Usuario{
    let usuario = new Usuario();
    usuario.nome = "Digus"
    usuario.email ="rdigus@gmail.com"

    return usuario
  }
}
